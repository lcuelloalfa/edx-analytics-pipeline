#!/bin/sh

# Script que ejecuta una consulta para obtener la vista de uso recursos de aprendizaje dado un curso

DATE=$(date +%d-%m-%Y)

SQL="
SELECT
    usuario.username AS username ,
    usuario.first_name AS first_name,
    usuario.last_name AS last_name,
    usuario.email AS email,
    cohorte.name AS cohorte,
    estructura.seccion_nombre AS seccion_nombre,
    estructura.subseccion_nombre AS subseccion_nombre,
    estructura.recurso_aprendizaje_tipo AS recurso_aprendizaje_tipo,
    COUNT( DISTINCT estructura.recurso_aprendizaje_id ) AS cantidad_total,
    COUNT( DISTINCT utilizados.block_id ) AS utilizados,
    COALESCE( posts.cantidad_post, 0 ) AS cantidad_post,
	( 
		CASE WHEN recurso_aprendizaje_tipo <> 'discussion'
		THEN COALESCE( MAX( \`utilizados\`.\`date\` ), '' )
		ELSE COALESCE( SUBSTRING(posts.ultimo_acceso, 0, 10), '')
	END) AS ultimo_acceso
FROM
    estructura_cursos AS estructura
    LEFT JOIN student_courseenrollment AS inscritos
            ON inscritos.course_id=estructura.curso_id
    LEFT JOIN uso_recursos AS utilizados
            ON utilizados.block_id = estructura.recurso_aprendizaje_id
            AND inscritos.user_id = utilizados.user_id
    INNER JOIN auth_user AS usuario
            ON usuario.id=inscritos.user_id
    LEFT JOIN posts_aggregation as posts
            ON posts.recurso_aprendizaje_id = estructura.recurso_aprendizaje_id
            AND posts.autor_id = inscritos.user_id
    INNER JOIN course_groups_courseusergroup as cohorte
            on cohorte.course_id = estructura.curso_id
    INNER JOIN course_groups_courseusergroup_users as estudiante
            on cohorte.id = estudiante.courseusergroup_id
            and usuario.id = estudiante.user_id
WHERE
	curso_id='$1'
GROUP BY
    usuario.username,
    usuario.first_name,
    usuario.last_name,
    usuario.email,
    cohorte.name,
    estructura.seccion_nombre,
    estructura.subseccion_nombre,
    estructura.recurso_aprendizaje_tipo,
	posts.ultimo_acceso,
    posts.cantidad_post
ORDER BY
    usuario.username,
    usuario.first_name,
    usuario.last_name,
    usuario.email,
    cohorte.name,
    estructura.seccion_nombre,
    estructura.subseccion_nombre,
    estructura.recurso_aprendizaje_tipo
"

CURSO=$(echo $1 | sed 's/.*://')

hive -e "set hive.cli.print.header=true; $SQL" | sed 's/[\t]/,/g' > uso_recursos_$CURSO\_$DATE.csv
