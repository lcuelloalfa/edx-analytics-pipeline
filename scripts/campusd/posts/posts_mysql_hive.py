import MySQLdb
from pymongo import MongoClient
import os

HOST='mysql'
PORT='port'
USER='pipeline001'
PASSWD='password'
DB='analytics'


def ejecutar_sql(sql):
    bd = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
            db=DB
        )
    cursor = bd.cursor()
    cursor.execute(sql)
    cursor.close ()
    bd.close()

def lista_post():
    sql = "DROP TABLE IF EXISTS posts"
    ejecutar_sql(sql)
    sql = """
        CREATE TABLE IF NOT EXISTS
        posts(
            post_id VARCHAR(255) NOT NULL,
            curso_id VARCHAR(50) NOT NULL,
            autor_id INT NOT NULL,
            autor_username VARCHAR(255) NOT NULL,
            foro_id VARCHAR(255),
            comment_thread_id VARCHAR(255),
            thread_type VARCHAR(50),
            cantidad_comentarios INT,
            fecha DATETIME,
            ultima_actividad DATETIME,
            titulo VARCHAR(255)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1
    """
    ejecutar_sql(sql)
    analytics = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
            db=DB
        )
    cursor_analytics = analytics.cursor()
    client = MongoClient('mongo', 27017)
    db = client['cs_comments_service']
    posts = db.contents
    for post in posts.find():
        post_id = post['_id']
        curso_id = post['course_id']
        autor_id = post['author_id']
        autor_username = post['author_username']
        fecha = post['created_at']
        try:
            ultima_actividad = post['last_activity_at']
        except:
            ultima_actividad = ''
        try:
            thread_type = post['thread_type']
        except:
            thread_type = ''
        try:
            commentable_id = post['commentable_id']
        except:
            commentable_id = ''
        try:
            comment_thread_id = str(post['comment_thread_id'])
        except:
            comment_thread_id = ''
        try:
            cantidad_comentarios = post['comment_count']
        except:
            cantidad_comentarios = None
        try:
            titulo = post['title'].encode('latin-1', 'ignore')
        except:
            titulo = ''
        sql = """
	    INSERT INTO posts (
            	post_id,
		curso_id,
		autor_id,
		autor_username,
		foro_id,
		comment_thread_id,
		thread_type,
		cantidad_comentarios,
		fecha,
                ultima_actividad,
		titulo
	    )
           VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
	"""
        cursor_analytics.execute(
                sql, (
                    post_id,
                    curso_id,
                    autor_id,
                    autor_username,
                    commentable_id,
                    comment_thread_id,
                    thread_type,
                    cantidad_comentarios,
                    fecha,
                    ultima_actividad,
                    titulo)
                )
    cursor_analytics.close()
    analytics.commit()
    analytics.close()

def foro_aggregator():
    sql = "DROP TABLE IF EXISTS posts_aggregation"
    ejecutar_sql(sql)

    sql = """
        CREATE TABLE IF NOT EXISTS posts_aggregation
            SELECT
                posts2.autor_id,
                posts2.autor_username,
                estructura.recurso_aprendizaje_id,
                count(posts1.post_id) AS cantidad_post,
                posts1.ultima_actividad AS ultimo_acceso
            FROM
                estructura_foros AS estructura
                LEFT JOIN posts AS posts1
                    ON estructura.foro_id = posts1.foro_id
                LEFT JOIN posts as posts2
                    ON posts1.post_id = posts2.comment_thread_id
            GROUP BY
                posts2.autor_id,
                posts2.autor_username,
                estructura.recurso_aprendizaje_id
            ORDER BY
                posts2.autor_username,
                estructura.recurso_aprendizaje_id
    """
    ejecutar_sql(sql)

def sqoop_import():
    drop = """
        DROP TABLE IF EXISTS posts;
        DROP TABLE IF EXISTS posts_aggregation;
    """
    cmd = """
        sqoop import-all-tables -m 1 \
            --connect jdbc:mysql://mysql:3306/analytics \
            --username pipeline001 \
            --password password \
            --hive-import \
            --hive-database default \
            --hive-overwrite \
            --create-hive-table \
            --exclude-tables estructura_cursos,estructura_foros,videos \
            --direct
        """
    os.system('hive -e "'+drop+'"')
    os.system(cmd)



def main():
    lista_post()
    foro_aggregator()
    sqoop_import()

if __name__ == '__main__':
    main()
