"""Categorize activity of users."""

""" Este archivo es una copia modificada de user_activity.py """

import datetime
import logging
import re
from collections import Counter

import luigi
import luigi.date_interval

import edx.analytics.tasks.util.eventlog as eventlog
from edx.analytics.tasks.common.mapreduce import MapReduceJobTaskMixin, MultiOutputMapReduceJobTask
from edx.analytics.tasks.common.mysql_load import MysqlInsertTask
from edx.analytics.tasks.common.pathutil import EventLogSelectionDownstreamMixin, EventLogSelectionMixin
from edx.analytics.tasks.insights.calendar_task import CalendarTableTask
from edx.analytics.tasks.util.decorators import workflow_entry_point
from edx.analytics.tasks.util.hive import BareHiveTableTask, HivePartitionTask, WarehouseMixin, hive_database_name
from edx.analytics.tasks.util.overwrite import OverwriteOutputMixin
from edx.analytics.tasks.util.url import get_target_from_url, url_path_join
from edx.analytics.tasks.util.weekly_interval import WeeklyIntervalMixin

log = logging.getLogger(__name__)

class ActividadUsuariosTask(OverwriteOutputMixin, WarehouseMixin, EventLogSelectionMixin, MultiOutputMapReduceJobTask):
    """
    Categorize activity of users.

    Analyze the history of user actions and categorize their activity. Note that categories are not mutually exclusive.
    A single event may belong to multiple categories. For example, we define a generic "ACTIVE" category that refers
    to any event that has a course_id associated with it, but is not an enrollment event. Other events, such as a
    video play event, will also belong to other categories.

    The output from this job is a table that represents the number of events seen for each user in each course in each
    category on each day.

    """

    output_root = None

    def mapper(self, line):
        value = self.get_event_and_date_string(line)
        if value is None:
            return
        event, date_string = value

        user_id = event.get('context', {}).get('user_id')
        if not user_id:
            self.incr_counter('UserActivity', 'Discard Missing User ID', 1)
            log.error("User-Activity: event without user_id in context: %s", event)
            return

        course_id = eventlog.get_course_id(event)
        if not course_id:
            return

        for label in self.get_predicate_labels(event):
            yield date_string, self._encode_tuple((str(user_id), course_id, date_string, label))

    def get_predicate_labels(self, event):
        """Creates labels by applying hardcoded predicates to a single event."""
        # We only want the explicit event, not the implicit form.
        event_type = event.get('event_type')
        event_source = event.get('event_source')

        re_curso = re.compile('/courses/course-v1:[a-zA-Z0-9_\+\-/*]*(info|course)/?')
        labels = []
        
        # Ignore all background task events, since they don't count as a form of activity.
        if event_source == 'task':
            return []

        # Ignore all enrollment events, since they don't count as a form of activity.
        if event_type.startswith('edx.course.enrollment.'):
            return []

        if re_curso.match(event_type):
            labels.append('acceso')

        if event_source == 'server':
            if event_type.startswith('edx.forum.') and event_type.endswith('.created'):
                labels.append('post')

        if event_source in ('browser', 'mobile'):
            if event_type == 'play_video':
                labels.append('play_video')

        return labels

    def _encode_tuple(self, values):
        """
        Convert values into a tuple containing encoded strings.

        Parameters:
            Values is a list or tuple.

        This enforces a standard encoding for the parts of the
        key. Without this a part of the key might appear differently
        in the key string when it is coerced to a string by luigi. For
        example, if the same key value appears in two different
        records, one as a str() type and the other a unicode() then
        without this change they would appear as u'Foo' and 'Foo' in
        the final key string. Although python doesn't care about this
        difference, hadoop does, and will bucket the values
        separately. Which is not what we want.
        """
        # TODO: refactor this into a utility function and update jobs
        # to always UTF8 encode mapper keys.
        if len(values) > 1:
            return tuple([value.encode('utf8') for value in values])
        else:
            return values[0].encode('utf8')

    def multi_output_reducer(self, _date_string, values, output_file):
        counter = Counter(values)

        for key, num_events in counter.iteritems():
            user_id, course_id, date_string, label = key
            value = (user_id, course_id, date_string, label, num_events)
            output_file.write('\t'.join([str(field) for field in value]))
            output_file.write('\n')

    def output_path_for_key(self, key):
        date_string = key
        return url_path_join(
            self.hive_partition_path('actividad_usuarios', date_string),
            'actividad_usuarios_{date}'.format(
                date=date_string,
            )
        )

    def run(self):
        # Remove the marker file.
        self.remove_output_on_overwrite()
        # Also remove actual output files in case of overwrite.
        if self.overwrite:
            for date in self.interval:
                url = self.output_path_for_key(date.isoformat())
                target = get_target_from_url(url)
                if target.exists():
                    target.remove()

        return super(ActividadUsuariosTask, self).run()


class ActividadUsuariosDownstreamMixin(WarehouseMixin, EventLogSelectionDownstreamMixin, MapReduceJobTaskMixin):
    """All parameters needed to run the ActividadUsuariosTableTask task."""

    overwrite_n_days = luigi.IntParameter(
        config_path={'section': 'user-activity', 'name': 'overwrite_n_days'},
        description='This parameter is used by ActividadUsuariosTask which will overwrite user-activity counts '
                    'for the most recent n days. Default is pulled from user-activity.overwrite_n_days.',
        significant=False,
    )


class ActividadUsuariosTableTask(ActividadUsuariosDownstreamMixin, BareHiveTableTask):
    """
    The hive table for storing user activity data. This task also adds partition metadata info to the Hive metastore.
    """

    date = luigi.DateParameter()
    interval = None

    def requires(self):
        # Overwrite n days of user activity data before recovering partitions.
        if self.overwrite_n_days > 0:
            overwrite_from_date = self.date - datetime.timedelta(days=self.overwrite_n_days)
            overwrite_interval = luigi.date_interval.Custom(overwrite_from_date, self.date)

            yield ActividadUsuariosTask(
                interval=overwrite_interval,
                warehouse_path=self.warehouse_path,
                n_reduce_tasks=self.n_reduce_tasks,
                overwrite=True,
            )

    def query(self):
        query = super(ActividadUsuariosTableTask, self).query()
        # Append a metastore check command with the repair option.
        # This will add metadata about partitions that exist in HDFS.
        return query + "MSCK REPAIR TABLE {table};".format(table=self.table)

    @property
    def table(self):
        return 'actividad_usuarios'

    @property
    def partition_by(self):
        return 'dt'

    @property
    def columns(self):
        return [
            ('user_id', 'INT'),
            ('course_id', 'STRING'),
            ('date', 'STRING'),
            ('category', 'STRING'),
            ('count', 'INT'),
        ]
