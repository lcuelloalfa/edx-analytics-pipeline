#!/bin/sh

# Importa las tablas posts y posts_aggregation desde mysql a hive

# Elimina las carpetas previamente creadas por el mismo script
hadoop fs -rm -r /user/hadoop/{posts,posts_aggregation}

SQL="
	DROP TABLE IF EXISTS posts;
	DROP TABLE IF EXISTS posts_aggregation;
	"
hive -e "$SQL"

sqoop import-all-tables -m 1 \
	--connect jdbc:mysql://mysql:3306/analytics \
	--username pipeline001 \
	--password password \
	--hive-import \
	--hive-database default \
	--hive-overwrite \
	--create-hive-table	\
	--exclude-tables estructura_cursos,estructura_foros,videos \
	--direct


