# Script que llama a vista_acceso_cursos_csv.sh para generar las vistas de acceso cursos

import MySQLdb
import os

HOST='mysql'
PORT=3306
USER='pipeline001'
PASSWD='password'
DB='analytics'

now = datetime.datetime.now()

sql='SELECT DISTINCT curso_id FROM estructura_cursos'
bd=MySQLdb.connect(host=HOST,
        port=PORT,
        user=USER,
        passwd=PASSWD,
        db=DB
    )
cursor=bd.cursor()
cursor.execute(sql)
cursos=cursor.fetchall()

for curso in cursos:
    print('====================EJECUTANDO ACCESO_CURSOS PARA EL CURSO '+curso[0]+'====================')
    cmd='sh vista_acceso_cursos_csv.sh '+curso[0]
    os.system(cmd)
cursor.close()
bd.close()
