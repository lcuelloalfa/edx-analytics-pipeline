# Script que llama a vista_uso_recursos_hive.sh para generar las vistas de uso recursos

import MySQLdb
import os

HOST='mysql'
PORT=3306
USER='pipeline001'
PASSWD='password'
DB='analytics'

sql='SELECT DISTINCT curso_id FROM estructura_cursos'
bd=MySQLdb.connect(host=HOST,
        port=PORT,
        user=USER,
        passwd=PASSWD,
        db=DB
    )
cursor=bd.cursor()
cursor.execute(sql)
cursos=cursor.fetchall()

for curso in cursos:
    print('====================EJECUTANDO CONSULTA PARA EL CURSO '+curso[0]+'====================')
    cmd='sh vista_uso_recursos_csv.sh '+curso[0]
    os.system(cmd)

cursor.close()
bd.close()

