# coding: utf-8
# In[ ]:

#!/usr/bin/python
from argparse import ArgumentParser, RawTextHelpFormatter
import json
import urllib
import logging
import MySQLdb
import gzip
import cStringIO
from pprint import pprint
import sys
from pymongo import MongoClient
import isodate
import os

HOST='mysql'
PORT=3306
USER='pipeline001'
PASSWD='password'
EDXAPP_DB='edxapp'
ANALYTICS_DB='analytics'

def sqoop_import():
    comando = """
        sqoop import-all-tables \
            -m 1 \
            --connect jdbc:mysql://"""+HOST+""":"""+str(PORT)+"""/"""+ANALYTICS_DB+""" \
            --username """+USER+""" \
            --password """+PASSWD+"""\
            --hive-import \
            --hive-database default \
            --create-hive-table \
            --direct \
    """
    drop = """
        DROP TABLE IF EXISTS estructura_cursos;
        DROP TABLE IF EXISTS estructura_foros;
        DROP TABLE IF EXISTS posts;
        DROP TABLE IF EXISTS posts_aggregation;
    """
    os.system('hive -e "'+drop+'"')
    os.system(comando)

def ejecutar_sql(sql):
    bd = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
            db=ANALYTICS_DB
        )
    cursor = bd.cursor()
    cursor.execute(sql)
    cursor.close ()
    bd.close()

def drop_bd():
    sql = "DROP DATABASE IF EXISTS analytics"
    bd = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
        )
    cursor = bd.cursor()
    cursor.execute(sql)

def crear_bd():
    sql = "CREATE DATABASE IF NOT EXISTS analytics"
    bd = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
        )
    cursor = bd.cursor()
    cursor.execute(sql)


def inicio_tablas( ):
    #crear tabla estructura cursos
    sql =  """
    CREATE TABLE IF NOT EXISTS estructura_cursos (
        curso_id VARCHAR(50) NOT NULL,
        curso_codigo VARCHAR(15) NOT NULL,
        curso_anio VARCHAR(4) NOT NULL,
        curso_semestre VARCHAR(2) NOT NULL,
        curso_nombre VARCHAR(200) NOT NULL,
        seccion_nombre VARCHAR(255) NOT NULL,
        subseccion_nombre VARCHAR(255) NOT NULL,
        recurso_aprendizaje_id VARCHAR(255) NOT NULL,
        recurso_aprendizaje_tipo VARCHAR(30) NOT NULL,
        recurso_aprendizaje_nombre VARCHAR(255) NOT NULL,
        recurso_aprendizaje_evaluado VARCHAR(5) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1
    """
    ejecutar_sql(sql)

    #crear tabla estructura foros
    sql = """
    CREATE TABLE IF NOT EXISTS estructura_foros (
        curso_id VARCHAR(50) NOT NULL,
        recurso_aprendizaje_id VARCHAR(255) NOT NULL,
        foro_id VARCHAR(255) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1
    """
    ejecutar_sql(sql)

"""
Helper function to reverse CompressedTextField.get_prep_value.
"""
def uncompress_string(s):
    try:
        val = s.decode('base64')
        zbuf = cStringIO.StringIO(val)
        zfile = gzip.GzipFile(fileobj=zbuf)
        ret = zfile.read()
        zfile.close()
    except Exception as e:
        log.error('String decompression failed. There may be corrupted data in the database: %s', e)
        ret = '{}'
    return ret


def lista_cursos( ):
    edxapp = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
            db=EDXAPP_DB
        )
    sql = "SELECT course_id, structure_json FROM course_structures_coursestructure"
    cursor_edxapp = edxapp.cursor()
    cursor_edxapp.execute(sql)
    cursos = cursor_edxapp.fetchall()
    cursor_edxapp.close()
    edxapp.close()
    analytics = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
            db=ANALYTICS_DB
        )
    cursor_analytics = analytics.cursor()
    for curso in cursos:
        estructura = json.loads(uncompress_string(curso[1]))
        blocks = estructura['blocks']
        for block in blocks:
            if blocks[block]['block_type'] == 'course':
                for seccion in blocks[block]['children']:
                    for subseccion in blocks[seccion]['children']:
                        for secuencia_aprendizaje in blocks[subseccion]['children']:
                            for recurso_aprendizaje in blocks[secuencia_aprendizaje]['children']:
                                try:
                                    curso_id = curso[0]
                                except:
                                    curso_id = ""
                                try:
                                    curso_codigo = curso_id.split('/')[1]
                                except:
                                    curso_codigo = ""
                                try:
                                    curso_anio = curso_id.split('/')[2].split('_')[0]
                                except:
                                    curso_anio = ""
                                try:
                                    curso_semestre = curso_id.split('/')[2].split('_')[1]
                                except:
                                    curso_semestre = ""
                                try:
                                    curso_nombre = blocks[block]['display_name'].encode('latin-1', 'ignore')
                                except:
                                    curso_nombre = ""
                                try:
                                    seccion_nombre = blocks[seccion]['display_name'].encode('latin-1', 'ignore')
                                except:
                                    seccion_nombre = ""
                                try:
                                    subseccion_nombre = blocks[subseccion]['display_name'].encode('latin-1', 'ignore')
                                except:
                                    subseccion_nombre = ""
                                try:
                                    recurso_aprendizaje_id = blocks[recurso_aprendizaje]['usage_key'].encode('latin-1', 'ignore')
                                except:
                                    recurso_aprendizaje_id = ""
                                try:
                                    recurso_aprendizaje_tipo = blocks[recurso_aprendizaje]['block_type'].encode('latin-1', 'ignore')
                                except:
                                    recurso_aprendizaje_tipo = ""
                                try:
                                    recurso_aprendizaje_nombre = blocks[recurso_aprendizaje]['display_name'].encode('latin-1', 'ignore')
                                except:
                                    recurso_aprendizaje_nombre = ""
                                try:
                                    recurso_aprendizaje_evaluado = str(blocks[recurso_aprendizaje]['graded'])
                                except:
                                    recurso_aprendizaje_evaluado = ""
                                sql = """
                                INSERT INTO estructura_cursos (
                                    curso_id,
                                    curso_codigo,
                                    curso_anio,
                                    curso_semestre,
                                    curso_nombre,
                                    seccion_nombre,
                                    subseccion_nombre,
                                    recurso_aprendizaje_id,
                                    recurso_aprendizaje_tipo,
                                    recurso_aprendizaje_nombre,
                                    recurso_aprendizaje_evaluado
                                ) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
                                cursor_analytics.execute(sql,(
                                        curso_id,
                                        curso_codigo,
                                        curso_anio,
                                        curso_semestre,
                                        curso_nombre,
                                        seccion_nombre,
                                        subseccion_nombre,
                                        recurso_aprendizaje_id,
                                        recurso_aprendizaje_tipo,
                                        recurso_aprendizaje_nombre,
                                        recurso_aprendizaje_evaluado
                                    )
                                )
    cursor_analytics.close()
    analytics.commit()
    analytics.close()


def lista_foros( ):
    edxapp = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
            db=EDXAPP_DB
        )
    sql = "SELECT course_id, discussion_id_map_json FROM course_structures_coursestructure"
    cursor_edxapp = edxapp.cursor()
    cursor_edxapp.execute(sql)
    cursos = cursor_edxapp.fetchall()
    cursor_edxapp.close()
    edxapp.close()
    analytics = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
            db=ANALYTICS_DB
        )
    cursor_analytics = analytics.cursor()
    for curso in cursos:
        estructura = json.loads(uncompress_string(curso[1]))
        try:
            curso_id = curso[0]
        except:
            curso_id = ""
        for key,value in estructura.iteritems():
            sql = """
            INSERT INTO estructura_foros (
                curso_id,
                recurso_aprendizaje_id,
                foro_id
            ) VALUES (%s,%s,%s)"""
            cursor_analytics.execute(sql, (curso_id,value,key))
    cursor_analytics.close()
    analytics.commit()
    analytics.close()

def lista_post():
    sql = """
	CREATE TABLE IF NOT EXISTS
	posts(
	    post_id VARCHAR(255) NOT NULL,
	    curso_id VARCHAR(50) NOT NULL,
  	    autor_id INT NOT NULL,
            autor_username VARCHAR(255) NOT NULL,
	    foro_id VARCHAR(255),
 	    comment_thread_id VARCHAR(255),
            thread_type VARCHAR(50),
	    cantidad_comentarios INT,
	    fecha DATETIME,
            ultima_actividad DATETIME,
	    titulo VARCHAR(255)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1
    """
    ejecutar_sql(sql)
    analytics = MySQLdb.connect(
            host=HOST,
            port=PORT,
            user=USER,
            passwd=PASSWD,
            db=ANALYTICS_DB
        )
    cursor_analytics = analytics.cursor()
    client = MongoClient('mongo', 27017)
    db = client['cs_comments_service']
    posts = db.contents
    for post in posts.find():
        post_id = post['_id']
        curso_id = post['course_id']
        autor_id = post['author_id']
        autor_username = post['author_username']
        fecha = post['created_at']
        try:
            ultima_actividad = post['last_activity_at']
        except:
            ultima_actividad = ''
        try:
            thread_type = post['thread_type']
        except:
            thread_type = ''
        try:
            commentable_id = post['commentable_id']
        except:
            commentable_id = ''
        try:
            comment_thread_id = str(post['comment_thread_id'])
        except:
            comment_thread_id = ''
        try:
            cantidad_comentarios = post['comment_count']
        except:
            cantidad_comentarios = None
        try:
            titulo = post['title'].encode('latin-1', 'ignore')
        except:
            titulo = ''
        sql = """
            INSERT INTO posts (
                post_id,
                curso_id,
                autor_id,
                autor_username,
                foro_id,
                comment_thread_id,
                thread_type,
                cantidad_comentarios,
                fecha,
                ultima_actividad,
                titulo
            ) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
        """
        cursor_analytics.execute(sql, (post_id, curso_id, autor_id, autor_username, commentable_id, comment_thread_id, thread_type, cantidad_comentarios, fecha, ultima_actividad, titulo))
    cursor_analytics.close()
    analytics.commit()
    analytics.close()

def foro_aggregator():

    sql = """
        CREATE TABLE IF NOT EXISTS posts_aggregation
            SELECT
                posts2.autor_id,
                posts2.autor_username,
                estructura.recurso_aprendizaje_id,
                count(posts1.post_id) AS cantidad_post,
                posts1.ultima_actividad AS ultimo_acceso
            FROM
                estructura_foros AS estructura
                LEFT JOIN posts AS posts1
                    ON estructura.foro_id = posts1.foro_id
                LEFT JOIN posts as posts2
                    ON posts1.post_id = posts2.comment_thread_id
            GROUP BY
                posts2.autor_id,
                posts2.autor_username,
                estructura.recurso_aprendizaje_id
            ORDER BY
                posts2.autor_username,
                estructura.recurso_aprendizaje_id
    """
    ejecutar_sql(sql)


'''
  Funcion principal
'''
def main():
    # Se configura archivo log
    #log = logging.getLogger(__name__)
    #log.setLevel(logging.DEBUG)
    drop_bd()
    crear_bd()
    inicio_tablas()
    lista_cursos()
    lista_foros()
    lista_post()
    foro_aggregator()
    sqoop_import()

if __name__ == '__main__':
    main()
