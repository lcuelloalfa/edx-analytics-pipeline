""" Categoriza el uso de recursos de los usuarios en cada curso """

""" Este archivo es una copia modificada de user_activity.py """

import datetime
import logging
import re
from collections import Counter

import luigi
import luigi.date_interval

import edx.analytics.tasks.util.eventlog as eventlog
from edx.analytics.tasks.common.mapreduce import MapReduceJobTaskMixin, MultiOutputMapReduceJobTask
from edx.analytics.tasks.common.mysql_load import MysqlInsertTask
from edx.analytics.tasks.common.pathutil import EventLogSelectionDownstreamMixin, EventLogSelectionMixin
from edx.analytics.tasks.insights.calendar_task import CalendarTableTask
from edx.analytics.tasks.util.decorators import workflow_entry_point
from edx.analytics.tasks.util.hive import BareHiveTableTask, HivePartitionTask, WarehouseMixin, hive_database_name
from edx.analytics.tasks.util.overwrite import OverwriteOutputMixin
from edx.analytics.tasks.util.url import get_target_from_url, url_path_join
from edx.analytics.tasks.util.weekly_interval import WeeklyIntervalMixin

import datetime

log = logging.getLogger(__name__)

class UsoRecursosTask(OverwriteOutputMixin, WarehouseMixin, EventLogSelectionMixin, MultiOutputMapReduceJobTask):
    output_root = None

    def mapper(self, line):
        value = self.get_event_and_date_string(line)
        if value is None:
            return
        event, date_string = value

        #dt = datetime.datetime.strptime(date_string, '%Y-%m-%d')
        #date_string = '{0}/{1}/{2:02}'.format(dt.day, dt.month, dt.year % 100)

        user_id = event.get('context', {}).get('user_id')
        if not user_id:
            self.incr_counter('UserActivity', 'Discard Missing User ID', 1)
            log.error("User-Activity: event without user_id in context: %s", event)
            return

        course_id = eventlog.get_course_id(event)
        if not course_id:
            return

        re_block_id = 'block-v1:[a-zA-Z0-9_\+\-@*]*'

        et = event.get('event_type')

        match = re.search(re_block_id, et)
        if match:
            block_id = match.group(0)
        else:
            return

        for label in self.get_predicate_labels(event):
            yield date_string, self._encode_tuple((str(user_id), course_id, block_id, date_string, label))

    def get_predicate_labels(self, event):
        """Creates labels by applying hardcoded predicates to a single event."""
        # We only want the explicit event, not the implicit form.
        event_type = event.get('event_type')
        event_source = event.get('event_source')

        re_tipo_recurso = re.compile('[a-zA-Z0-9_\+\-\/:@]*block@[a-zA-Z0-9]*')

        re_search_recurso = '@(.+?)\+'

        labels = []
        # Ignore all background task events, since they don't count as a form of activity.
        if event_source == 'task':
            return []

        if event_source == 'server':
            if event_type.startswith('edx.forum.') and event_type.endswith('.created'):
                labels.append('post')

        # Ignore all enrollment events, since they don't count as a form of activity.
        if event_type.startswith('edx.course.enrollment.'):
            return []

        if re_tipo_recurso.match(event_type):
            match = re.search(re_search_recurso, event_type).group(1)
            labels.append(match)

        return labels

    def _encode_tuple(self, values):

        # TODO: refactor this into a utility function and update jobs
        # to always UTF8 encode mapper keys.
        if len(values) > 1:
            return tuple([value.encode('utf8') for value in values])
        else:
            return values[0].encode('utf8')

    def multi_output_reducer(self, _date_string, values, output_file):
        counter = Counter(values)

        for key, num_events in counter.iteritems():
            user_id, course_id, block_id,  date_string, label = key
            value = (user_id, course_id, block_id, date_string, label, num_events)
            output_file.write('\t'.join([str(field) for field in value]))
            output_file.write('\n')

    def output_path_for_key(self, key):
        date_string = key
        return url_path_join(
            self.hive_partition_path('uso_recursos', date_string),
            'uso_recursos_{date}'.format(
                date=date_string,
            )
        )

    def run(self):
        # Remove the marker file.
        self.remove_output_on_overwrite()
        # Also remove actual output files in case of overwrite.
        if self.overwrite:
            for date in self.interval:
                url = self.output_path_for_key(date.isoformat())
                target = get_target_from_url(url)
                if target.exists():
                    target.remove()

        return super(UsoRecursosTask, self).run()


class UsoRecursosDownstreamMixin(WarehouseMixin, EventLogSelectionDownstreamMixin, MapReduceJobTaskMixin):
    """All parameters needed to run the UsoRecursosTableTask task."""

    overwrite_n_days = luigi.IntParameter(
        config_path={'section': 'user-activity', 'name': 'overwrite_n_days'},
        description='This parameter is used by UsoRecursosTask which will overwrite user-activity counts '
                    'for the most recent n days. Default is pulled from user-activity.overwrite_n_days.',
        significant=False,
    )


class UsoRecursosTableTask(UsoRecursosDownstreamMixin, BareHiveTableTask):
    """
    The hive table for storing uso recursos data. This task also adds partition metadata info to the Hive metastore.
    """

    date = luigi.DateParameter()
    interval = None

    def requires(self):
        # Overwrite n days of user activity data before recovering partitions.
        if self.overwrite_n_days > 0:
            overwrite_from_date = self.date - datetime.timedelta(days=self.overwrite_n_days)
            overwrite_interval = luigi.date_interval.Custom(overwrite_from_date, self.date)

            yield UsoRecursosTask(
                interval=overwrite_interval,
                warehouse_path=self.warehouse_path,
                n_reduce_tasks=self.n_reduce_tasks,
                overwrite=True,
            )

    def query(self):
        query = super(UsoRecursosTableTask, self).query()
        # Append a metastore check command with the repair option.
        # This will add metadata about partitions that exist in HDFS.
        return query + "MSCK REPAIR TABLE {table};".format(table=self.table)

    @property
    def table(self):
        return 'uso_recursos'

    @property
    def partition_by(self):
        return 'dt'

    @property
    def columns(self):
        return [
            ('user_id', 'INT'),
            ('course_id', 'STRING'),
            ('block_id', 'STRING'),
            ('date', 'STRING'),
            ('category', 'STRING'),
            ('count', 'INT'),
        ]

