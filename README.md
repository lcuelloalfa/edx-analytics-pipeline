edX Analytics Pipeline
======================
Este proyecto es un fork de [edx-analytics-pipeline](https://github.com/edx/edx-analytics-pipeline) para campus digital, se agregaron dos tareas: `ActividadUsuariosTableTask` y `UsoRecursosTableTask` y una serie de scripts para desplegar las vistas de acceso a cursos desagregada por estudiante y uso de recursos de aprendizaje desagregado por estudiante y seccion del curso para los profesores.

Módulo para el análisis de los datos de la plataforma **Open edX**. Es un motor de análisis en batch capaz de ejecutar flujos de trabajo complejos para el procesamiento de datos.

El **pipeline** toma grandes cantidades de datos crudos, los analiza y produce outputs de alto valor que son utilizados por varias herramientas downstream.

El consumidor principal de estos datos es [Open edX Insights](http://edx.readthedocs.io/projects/edx-insights/en/latest/).

Tambien es utilizado para generar una variedad de outputs empaquetados para investigación, inteligencia de negocios y otro tipo de reportes.

Recoje los datos desde una variedad de fuentes incluyendo las siguientes:

* Archivos de [tracking log](http://edx.readthedocs.io/projects/devdata/en/latest/internal_data_formats/event_list.html) - Esta es la fuente primaria de los datos
* Base de datos **LMS** 
* Base de datos **Otto** 
* *APIs* de **LMS** (estructura de los cursos, listado de los cursos)

Produce salidas a los siguientes destinos:

* **S3** - Reportes *CSV*, exportaciones empaquetadas
* **MySQL** - Este es conocido como el *"result store"* y es utilizado por **Insights**
* **Elasticsearch** - Es utilizado por **Insights**
* **Vertica** - Es usado para business intelligence y para producir reportes

Este módulo utiliza [spotify/luigi](https://github.com/spotify/luigi) como el núcleo del *workflow engine*

La transformación de datos y el análisis es realizado con la ayuda de las siguientes herramientas *third-party* (entre otras):

* Python
* [Pandas](http://pandas.pydata.org/)
* [Hive](https://hive.apache.org/)
* [Hadoop](http://hadoop.apache.org/)
* [Sqoop](http://sqoop.apache.org/)

El *pipeline* está diseñado para ser invocado periódicamente por un scheduler externo. Este puede ser **cron**, **jenkins** o cualquier otro sistema que pueda correr comandos *shell* periódicamente.

Esta es una vista simplificada, de alto nivel de la arquitectura:

![Diagrama arquitectónico Open edX Analytics](http://edx.readthedocs.io/projects/edx-installing-configuring-and-running/en/latest/_images/Analytics_Pipeline.png)


