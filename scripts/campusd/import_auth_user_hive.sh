#!/bin/sh

# Script para importar la tabla auth_user a hive

# Elimina el directorio previamente creado 
hadoop fs -rm -r /user/hadoop/auth_user

DROP="DROP TABLE IF EXISTS auth_user"

# Elimina la tabla si existe
hive -e "$DROP"

# Consulta SQL para obtener la tabla auth_user, si no ponia WHERE $CONDITIONS daba error
QUERY="SELECT id, username, first_name, last_name, email, date_joined FROM auth_user WHERE \$CONDITIONS"

sqoop import -m 1 \
	--connect jdbc:mysql://mysql:3306/edxapp \
	--username pipeline001 \
	--password password \
	--hive-import \
	--hive-database default \
	--query "$QUERY" \
	--target-dir hdfs://namenode:8020/user/hadoop/auth_user/ \
	--hive-overwrite \
	--hive-table auth_user \
	--create-hive-table \
	--direct
