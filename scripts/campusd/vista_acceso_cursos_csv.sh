#!/bin/sh

# Script para generar los archivos csv con los accesos de los usuarios a los cursos

DATE=$(date +%d-%m-%Y)

SQL="
SELECT
    \`accesos\`.\`date\` as fecha_acceso,
    accesos.user_id as student_id,
    usuario.username as username,
	usuario.first_name as first_name,
    usuario.last_name as last_name,
    usuario.email as email,
    cohorte.name as cohorte
FROM
    actividad_usuarios AS accesos
    INNER JOIN auth_user AS usuario
 	ON usuario.id = accesos.user_id
    INNER JOIN course_groups_courseusergroup as cohorte
	ON cohorte.course_id = accesos.course_id
    INNER JOIN course_groups_courseusergroup_users as estudiante
	ON cohorte.id = estudiante.courseusergroup_id
	AND usuario.id = estudiante.user_id
WHERE 
	accesos.course_id = '$1'
GROUP BY
    \`accesos\`.\`date\`,
    accesos.user_id,
    usuario.username,
    usuario.first_name,
    usuario.last_name,
    usuario.email,
    cohorte.name
"

# Se elimina el prefijo 'course-v1:'
CURSO=$(echo $1 | sed 's/.*://')

# Setea los headers del csv, luego ejecuta la consulta en hive y la guarda en el archivo .csv
hive -e "set hive.cli.print.header=true; $SQL" | sed 's/[\t]/,/g' > acceso_cursos_$CURSO\_$DATE.csv
