# Importa las tablas de la bd analytics (estructura.py) desde mysql a hive

# Elimina las tablas en hive
SQL="DROP TABLE IF EXISTS estructura_cursos; 
	DROP TABLE IF EXISTS estructura_foros;
   	DROP TABLE IF EXISTS videos;
	DROP TABLE IF EXISTS posts;
	DROP TABLE IF EXISTS posts_aggregation;
	"

# Ejecuta el comando SQL en hive
hive -e "$SQL"

# Comando Sqoop para importar las tablas de la bd analytics 
sqoop import-all-tables -m 1 \
	--connect jdbc:mysql://mysql:3306/analytics \
	--username pipeline001 \
	--password password \
	--hive-import \
	--hive-database default \
	--create-hive-table \
	--direct
