launch-task ImportCourseUserGroupTask --local-scheduler --overwrite
launch-task ImportCourseUserGroupUsersTask --local-scheduler --overwrite
launch-task ImportStudentCourseEnrollmentTask --local-scheduler --overwrite
/edx/app/analytics_pipeline/analytics_pipeline/scripts/campusd/import_auth_user_hive.sh
(cd /edx/app/analytics_pipeline/analytics_pipeline && launch-task ActividadUsuariosTableTask --local-scheduler --overwrite --date $(date +%Y-%m-%d) --overwrite-n-days 30)
(cd /edx/app/analytics_pipeline/analytics_pipeline && launch-task UsoRecursosTableTask --local-scheduler --overwrite --date $(date +%Y-%m-%d) --overwrite-n-days 30)
